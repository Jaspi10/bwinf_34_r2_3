public class Kante {
	
	//Start und ziel (nicht �nderbar)
	public final Knoten start, ziel;

	//Konstruktor
	public Kante(Knoten start, Knoten ziel) {
		this.start = start;
		this.ziel = ziel;
	}
}
