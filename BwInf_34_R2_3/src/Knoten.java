import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Knoten {
	
	//Kanten (Ein- und Ausgehende)
	private List<Kante> kanten = new ArrayList<Kante>();
	
	//Farbe des Knoten
	private int farbe = 0;
	
	//Position in der Welt
	public final int x, y;

	//Konstruktor
	public Knoten(int x, int y) {
		this.x = x;
		this.y = y;
	}


	// F�gt neue Kante zu diesem Knoten hinzu
	// Achtung: Muss an beiden Enden aufgerufen werden
	public void addKante(Knoten k, boolean ausgehend) {
		Kante neueKante;

		if (ausgehend) {
			neueKante = new Kante(this, k);
		} else {
			neueKante = new Kante(k, this);
		}

		kanten.add(neueKante);
	}

	//Liefert alle Kante, die this als start haben
	public List<Kante> getAusgehende() {
		List<Kante> rv = new LinkedList<Kante>();

		for (Kante k : kanten) {
			if (k.start.equals(this)) {
				rv.add(k);
			}
		}

		return rv;
	}

	//Liefert alle Knoten, die this als ziel haben
	public List<Kante> getEingehende() {
		List<Kante> rv = new LinkedList<Kante>();

		for (Kante k : kanten) {
			if (k.ziel.equals(this)) {
				rv.add(k);
			}
		}

		return rv;
	}

	//Setzt die farbe des Knoten auf farbe
	public void setFarbe(int farbe) {
		this.farbe = farbe;
	}
	
	public int getFarbe() {
		return this.farbe;
	}
}
