import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Graph {
	private HashMap<Integer, Knoten> map;
	private final int weltX, weltY;

	// Erstellt einen Graph aus einer Welt
	public Graph(Welt w) {

		System.out.println("Erstelle Graph");

		map = new HashMap<Integer, Knoten>();

		weltX = w.sizeX;
		weltY = w.sizeY;

		// F�ge alle Felder, die errewichbar sind als Knoten hinzu
		for (int i = 0; i < w.sizeY; i++) {
			for (int j = 0; j < w.sizeX; j++) {
				if (w.istErreichbar(j, i) && w.getFeld(j, i) != 2) {
					addKnoten(new Knoten(j, i));
				}
			}
		}

		// F�ge einen Knoten f�r ALLE ausg�nge hinzu
		Knoten ausgang = new Knoten(-1, 0); // Da der Ausgang den Key -1 hat
											// ist er an Position (-1|0)
		addKnoten(ausgang);
		ausgang.setFarbe(1); // Alle Ausg�nge haben die Farbe 1

		// F�ge Kanten hinzu
		for (Knoten k : map.values()) {

			int distanz = 0;

			if (k.getFarbe() == 1)
				continue;

			// Norden
			distanz = w.getDistanz(k.x, k.y, 'N');
			if (distanz > 0) {
				k.addKante(getKnoten(k.x, k.y - distanz), true);
				getKnoten(k.x, k.y - distanz).addKante(k, false);
			} else if (distanz == -1) {
				k.addKante(map.get(-1), true);
				map.get(-1).addKante(k, false);
			}

			// Osten
			distanz = w.getDistanz(k.x, k.y, 'O');
			if (distanz > 0) {
				k.addKante(getKnoten(k.x + distanz, k.y), true);
				getKnoten(k.x + distanz, k.y).addKante(k, false);
			} else if (distanz == -1) {
				k.addKante(map.get(-1), true);
				map.get(-1).addKante(k, false);
			}

			// Sueden
			distanz = w.getDistanz(k.x, k.y, 'S');
			if (distanz > 0) {
				k.addKante(getKnoten(k.x, k.y + distanz), true);
				getKnoten(k.x, k.y + distanz).addKante(k, false);
			} else if (distanz == -1) {
				k.addKante(map.get(-1), true);
				map.get(-1).addKante(k, false);
			}

			// Westen
			distanz = w.getDistanz(k.x, k.y, 'W');
			if (distanz > 0) {
				k.addKante(getKnoten(k.x - distanz, k.y), true);
				getKnoten(k.x - distanz, k.y).addKante(k, false);
			} else if (distanz == -1) {
				k.addKante(map.get(-1), true);
				map.get(-1).addKante(k, false);
			}
		}

	}

	// Setzt die Farbe von jedem Knoten, der zuErreichen erreichen kann, auf
	// farbe
	// WICHTIG: Funktioniert nur korrekt wenn noch kein Knoten farbe als Farbe
	// hat
	public void faerbeKnotenDieErreichenKoennen(Knoten zuErreichen, int farbe) {
		System.out.println("Faerbe Knoten die "
				+ "(" + zuErreichen.x + "|" + zuErreichen.y + ") "
						+ "erreichen koennen " + farbe);

		// Breitensuche mit Queue
		Queue<Knoten> queue = new ArrayDeque<Knoten>();
		queue.add(zuErreichen);
		while (!queue.isEmpty()) {
			Knoten knoten = queue.remove();
			for (Kante k : knoten.getEingehende()) {
				// Suche nicht weiter wenn ein Knoten schon farbe hat
				if (k.start.getFarbe() != farbe) {
					k.start.setFarbe(farbe);
					queue.add(k.start);
				}
			}
		}

	}

	public Knoten getAusgang() {
		return map.get(-1);
	}

	// Liefert alle Knoten, die farbe haben
	public List<Knoten> getKnotenMitFarbe(int farbe) {
		List<Knoten> rv = new LinkedList<Knoten>();

		for (Knoten k : map.values()) {
			if (k.getFarbe() == farbe) {
				rv.add(k);
			}
		}

		return rv;
	}

	// F�gt einen Knoten hinzu
	public void addKnoten(Knoten k) {
		map.put(getKey(k.x, k.y), k);
	}

	// Liefert den Knoten, der das Feld (x|y) repr�sentiert
	public Knoten getKnoten(int x, int y) {

		return map.get(getKey(x, y));

	}

	// Liefert den Key f�r das Feld (x|y)
	private int getKey(int x, int y) {
		return x + weltX * y;
	}
}
