import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;

public class Welt {

	// Array mit Feldern der Welt
	private int[][] welt; // 0 = Frei; 1 = Wand; 2 = Ausgang; 3 = Sicher

	// Gr��e der Welt
	final int sizeX, sizeY;

	// Konstruktor
	public Welt(File f) {

		System.out.println("Lese Welt aus " + f.getAbsolutePath());

		// tempor�rer Speicher f�r die gr��e
		int tmpSizeX = 0, tmpSizeY = 0;

		// Zwischenspeicher f�r zeilen w�hrend die gr��e bestimmt wird
		ArrayDeque<String> queue = new ArrayDeque<String>();

		// Bestimme gr��e
		try {
			BufferedReader in = new BufferedReader(new FileReader(f));
			String line = in.readLine();

			// X gr��e ist die L�nge einer Zeile
			tmpSizeX = line.length();
			tmpSizeY = 0;

			// Speihere Zeilen und erh�he Y gr��e
			while (line != null) {
				tmpSizeY++;
				queue.add(line);
				line = in.readLine();
			}

			in.close();

		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}

		// Erstelle Welt
		welt = new int[tmpSizeX][tmpSizeY];

		// F�r alle Felder der Welt
		for (int i = 0; i < tmpSizeY; i++) {
			String reihe = queue.remove();
			for (int j = 0; j < tmpSizeX; j++) {
				switch (reihe.charAt(j)) {
				// Leeres Feld
				case ' ':
					welt[j][i] = 0;
					break;
				// Wand
				case '#':
					welt[j][i] = 1;
					break;
				// Ausgang
				case 'E':
					welt[j][i] = 2;
					break;
				}
			}
		}

		// Gr��e
		this.sizeX = tmpSizeX;
		this.sizeY = tmpSizeY;
	}

	// Gibt die Distanz zum Endpunkt zur�ck
	// wenn 0 ist eine Wand im Weg
	// wenn -1 wird ein Ausgang erreicht
	// wenn -2 ERROR
	public int getDistanz(int x, int y, char richtung) {
		// Bewegung pro zug
		int xBeweg = 0;
		int yBeweg = 0;

		// Setze bewegung pro Zug
		switch (richtung) {
		// Norden
		case 'N':
			yBeweg = -1;
			break;
		// Osten
		case 'O':
			xBeweg = 1;
			break;
		// Sueden
		case 'S':
			yBeweg = 1;
			break;
		// Westen
		case 'W':
			xBeweg = -1;
			break;
		}

		boolean fertig = false;

		// Entfernung zum neuen Standpunkt
		int distanz = 0;

		// Solange nicht bei Wand oder Ausgang
		while (!fertig) {

			// Bewege in richtung
			x += xBeweg;
			y += yBeweg;

			if (welt[x][y] == 1) {
				// Wand
				return distanz;
			} else if (welt[x][y] == 2) {
				// Ausgang
				return -1;
			}

			// Ist einen weiteren Schritt gegangen
			distanz++;
		}

		// Error
		return -2;
	}

	// Pr�ft das Yamyam nach seiner ersten Richtungs�nderung auf dem Feld (x|y)
	// sein kann
	public boolean istErreichbar(int x, int y) {

		// Ausg�nge sind nicht ereichbar
		if (getFeld(x, y) == 1)
			return false;

		boolean wandNorden = false, wandOsten = false, wandSueden = false, wandWesten = false;

		int waende = 0;

		// Ist eine Wand dierekt n�rdlich von (x|y)
		if (getFeld(x, y - 1) == 1) {
			wandNorden = true;
			waende++;
		}

		// Ist eine Wand dierekt �stlich von (x|y)
		if (getFeld(x + 1, y) == 1) {
			wandOsten = true;
			waende++;
		}

		// Ist eine Wand dierekt s�dlich von (x|y)
		if (getFeld(x, y + 1) == 1) {
			wandSueden = true;
			waende++;
		}

		// Ist eine Wand dierekt westlich von (x|y)
		if (getFeld(x - 1, y) == 1) {
			wandWesten = true;
			waende++;
		}

		// Ist die Anzahl der W�nde ungerade oder gleich 2 und gegen�berliegend
		if (waende % 2 == 1 || (waende == 2 && !(wandNorden && wandSueden) && !(wandOsten && wandWesten))) {
			return true;
		} else {
			return false;
		}

	}

	// Setze alle sicheren Felder auf 3
	public void bestimmeSichereFelder() {

		// Generiere den Graphen
		Graph graph = new Graph(this);

		// F�rbe Knoten, von denen ein Ausgang erreichbar ist zwei
		graph.faerbeKnotenDieErreichenKoennen(graph.getAusgang(), 2);

		// F�rbe alle Knoten, von denen aus ein Feld, das nicht den Ausgang
		// erreichen kann, erreihbar ist 3
		for (Knoten k : graph.getKnotenMitFarbe(0)) {
			graph.faerbeKnotenDieErreichenKoennen(k, 3);
		}

		System.out.println("Bestimme sichere Felder mithilfe des Graphen");

		// Setzte alle sicheren Felder (mit Farbe 2) auf sicher
		for (Knoten k : graph.getKnotenMitFarbe(2)) {
			welt[k.x][k.y] = 3;
		}

		// Pr�fe f�r die Felder, die nicht im Graph sind, ob sie sicher sind
		for (int i = 0; i < this.sizeX; i++) {
			for (int j = 0; j < this.sizeY; j++) {

				// Ist das Feld ein Ausgang oder eine Wand
				if (welt[i][j] != 0)
					continue;

				// Ist das Feld teil des Graphen
				if (graph.getKnoten(i, j) != null)
					continue;

				// Springe zum anfang, wenn ein vom aktuellen Feld erreichbares
				// Feld nicht sicher, oder das Feld komplett umrundet ist

				int distanz;

				// Norden
				distanz = getDistanz(i, j, 'N');
				if (welt[i][j - distanz] == 0 && distanz > 0)
					continue;

				// Osten
				distanz = getDistanz(i, j, 'O');
				if (welt[i + distanz][j] == 0 && distanz > 0)
					continue;

				// Sueden
				distanz = getDistanz(i, j, 'S');
				if (welt[i][j + distanz] == 0 && distanz > 0)
					continue;

				// Westen
				distanz = getDistanz(i, j, 'W');
				if (welt[i - distanz][j] == 0 && distanz > 0)
					continue;

				// Ist das Feld komplett umrundet komplett umrundet
				if (welt[i + 1][j] == 1 && welt[i - 1][j] == 1 && welt[i][j + 1] == 1 && welt[i][j - 1] == 1)
					continue;

				welt[i][j] = 3;
			}
		}

	}

	// Gebe die Welt nach System.out aus
	public void printWelt() {
		for (int i = 0; i < this.sizeY; i++) {
			for (int j = 0; j < this.sizeX; j++) {
				char c = '?';
				switch (welt[j][i]) {
				case 0:
					c = ' ';
					break;
				case 1:
					c = '#';
					break;
				case 2:
					c = 'E';
					break;
				// Sichere Felder werden mit 'S' repr�sentiert
				case 3:
					c = 'S';
					break;
				}

				System.out.print(c);
			}
			System.out.print("\n");
		}
	}

	public int getFeld(int x, int y) {
		return welt[x][y];
	}
}
