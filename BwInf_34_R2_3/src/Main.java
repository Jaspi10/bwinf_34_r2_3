import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		//Datei lesen
		
		String pfad = null;

		//Argument vorhanden?
		if (args.length == 1) {
			pfad = args[0];
		} else {
			//Lese Pfad aus System.in
			BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

			try {
				pfad = r.readLine();
				r.close();
			} catch (IOException e) {

				e.printStackTrace();
			}

		}

		//Welt aus Datei generieren
		Welt w = new Welt(new File(pfad));

		//Berechnen der Sicheren Felder
		w.bestimmeSichereFelder();

		//Ausgabe
		w.printWelt();

	}

}
